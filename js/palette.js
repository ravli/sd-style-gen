function luma(color) {
    var rgb = color.toRgb();
    return (0.2126 * rgb.r + 0.7152 * rgb.g + 0.0722 * rgb.b) / 255;
}

function sat(color) {
    return color.toHsl().s
}

function light(color) {
    return color.toHsl().l
}

function luma_sort(colors, key_func) {
    return _.sortBy(colors, function(c){
        var color_luma = luma(c);
        var value = key_func(c, color_luma);
        console.log(c.toHexString(), value);
        return 0 - value;
    });
}

function updatePalette(data) {
    var colors = _.map(data[0].colors, function(c) { return tinycolor(c); });

    // @brand-cc1 - самый насыщенный и яркий цвет после @brand-cc2.
    // @brand-cc2 - должен быть самый насыщенный и самый близкий к 50% lightness самым ярким в палитре. ближе к 0/360 градусам по hue
    // @brand-cc3 - должен быть самый светлый в палитре и самый ненасыщенный, если есть два похожих по светлости +/- 10%.
    // @brand-cc4 - должен быть самый темный в палитре и самый ненасыщенный, если есть два похожих по темноте +/- 10%.

    console.log('Searching light');
    var pale_light_sorted = luma_sort(colors, function(c, color_luma){ return (1 - c.toHsl().s) * 0.3 + color_luma });
    var color3 = pale_light_sorted[0];
    colors = _.without(colors, color3);

    console.log('Searching dark');
    var pale_dark_sorted = luma_sort(colors, function(c, color_luma){ return (1 - c.toHsl().s) * 0.3 + (1 - color_luma) });
    var color4 = pale_dark_sorted[0];
    colors = _.without(colors, color4);

    console.log('Searching saturated red');
    var hsl_sorted = luma_sort(colors, function(c, color_luma){
        return c.toHsl().s / 4 + Math.abs(180 - c.toHsl().h) / 360 + (0.5 - Math.abs(0.5 - color_luma))
    });
    var color2 = hsl_sorted[0];
    colors = _.without(colors, color2);

    console.log('Searching second saturated');
    var sl_sorted = luma_sort(colors, function(c, color_luma){ return c.toHsl().s / 4 + color_luma });
    var color1 = sl_sorted[0];
    colors = _.without(colors, color1);

    var color5 = colors && colors[0] || color1;

    var i;

    // each modification tries 10 iterations to bring color to target coefficient

/*
    for (i=0; i < 10 && light(color3) < 0.7; i++) { color3 = tinycolor.lighten(color3, 40 * (1 - light(color3))) } // too dark
    for (i=0; i < 10 && sat(color3) > 0.5; i++) { color3 = tinycolor.desaturate(color3, 40 * sat(color3)) } // too saturated

    for (i=0; i < 10 && light(color4) > 0.4; i++) { color4 = tinycolor.darken(color4, 40 * light(color4)) } // too light
    for (i=0; i < 10 && sat(color4) > 0.5; i++) { color4 = tinycolor.desaturate(color4, 40 * sat(color4)) } // too saturated

    for (i=0; i < 10 && sat(color2) < 0.6; i++) { color2 = tinycolor.saturate(color2, 40 * (1 - sat(color2))) } // too pale
    for (i=0; i < 10 && light(color2) < 0.3; i++) { color2 = tinycolor.lighten(color2, 40 * light(color2)) } // too dark
    for (i=0; i < 10 && light(color2) > 0.7; i++) { color2 = tinycolor.darken(color2, 40 * light(color2)) } // too light
*/

    // You can modify colors here using http://bgrins.github.io/TinyColor/ features
    // for example, making color1 and color2 darker
    // color1 = tinycolor.desaturate(tinycolor.darken(color1, 20), 10);
    // color2 = tinycolor.darken(color2, 10);

    // color4 and color5 lighter
    // color4 = tinycolor.lighten(color4, 10);
    // color5 = tinycolor.desaturate(tinycolor.lighten(color5, 20));

    // Also, there are LESS functions
    // contrast_hue(color, red, blue, [threshold=180])
    // contrast_saturation(color, saturated, pale, [threshold=0.5])

    var vars = {
        '@brand-cc1': color1.toHexString(),
        '@brand-cc2': color2.toHexString(),
        '@brand-cc3': color3.toHexString(),
        '@brand-cc4': color4.toHexString(),
        '@brand-cc5': color5.toHexString()
    }
    less.modifyVars(vars);
}

$('#applyPalette').submit(function () {
    var url = 'http://www.colourlovers.com/api/palette/' + $('#paletteCode').val() + '?jsonCallback=updatePalette';
    $.ajax({
        url: url,
        dataType: 'jsonp'
    });
    return false;
});

$('#applyPalette').submit();